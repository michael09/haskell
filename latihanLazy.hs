import Data.List
-- 1. Uraikan langkah evaluasi dari ekspresi berikut: [ x+y | x <- [1 .. 4], y <- [2 .. 4], x > y ]
{-
evaluasi 1: generate list xs
evaluasi 2: generate list ys
eval 3: cek setiap element dari xs dan ys apakah ada yg memenuhi x > y
-}

-- 2. 
divisor n = [x| x <- [1..n], n `mod` x == 0]

--3.
quicksort [] = []
quicksort [x] = [x]
quicksort (x:xs) = quicksort [z | z <- xs, z <= x] ++ [x] ++ quicksort [y| y <- xs, y > x]

--4.
perms [] = [[]]
perms ls = [x:ps | x <- ls, ps <- perms (ls\\[x])]

--5. Sieve of Erastothenes
sieve = helpSieve [2..]
    where helpSieve(x:xs) = x : (helpSieve [z | z <- xs , z `mod` x /= 0])

--6.
pythaTriple = [(x,y,z) | z <- [5..]
                        , y <- [2 .. z-1]
                        , x <- [2 .. y-1]
                        , x*x + y*y == z*z]
