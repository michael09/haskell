data Expr = C Float | Expr :+ Expr | Expr :- Expr | Expr :* Expr | Expr :/ Expr | V String | let String Expr Expr
            deriving Show

subst :: String -> Expr -> Expr -> Expr
subst v0 e0 (V v1) = if (v0 == v1) then e0 else (V v1)
subst _ _ (C c) = (C c)
subst v0 e0 (e1 :+ e2) = subst v0 e0 e1 :+ subst v0 e0 e2
subst v0 e0 (e1 :- e2) = subst v0 e0 e1 :- subst v0 e0 e2
subst v0 e0 (e1 :* e2) = subst v0 e0 e1 :* subst v0 e0 e2
subst v0 e0 (e1 :/ e2) = subst v0 e0 e1 :/ subst v0 e0 e2
subst v0 e0 (Let v1 e1 e2) = Let v1 e1 (subst v0 e0 e2)


foldArith(a, s, m, d) (C x) = x
foldArith(a, s, m, d) (e1 :+ e2) = a (foldArith(a, s, m, d) e1) (foldArith(a, s, m, d) e2)
foldArith(a, s, m, d) (e1 :- e2) = s (foldArith(a, s, m, d) e1) (foldArith(a, s, m, d) e2)
foldArith(a, s, m, d) (e1 :* e2) = m (foldArith(a, s, m, d) e1) (foldArith(a, s, m, d) e2)
foldArith(a, s, m, d) (e1 :/ e2) = d (foldArith(a, s, m, d) e1) (foldArith(a, s, m, d) e2)
foldArith(a, s, m, d) (e1 :/ e2) = str (foldArith(a, s, m, d) e1) (foldArith(a, s, m, d) e2)

evalFold = foldArith(a, s, m, d)
    where 
        a = (+) 
        s = (-)
        m = (*)
        d = (/)
        str = evalFold(subst v e1 e2)

eval :: Expr -> Float
eval (C x)          = x
eval (e1 :+ e2)     = eval e1 + eval e2
eval (e1 :- e2)     = eval e1 - eval e2
eval (e1 :* e2)     = eval e1 * eval e2
eval (e1 :/ e2)     = eval e1 / eval e2
eval (Let v e0 e1) = eval (subst v e0 e1)
eval (V _) = 0.0


-- Count const
foldConstanta(o, z, f) (C x) = o
foldConstanta (o, z, f) (e1 :+ e2) = (foldConst (o, z, f) e1) + (foldConst (o, z, f) e2)
foldConstanta (o, z, f) (e1 :- e2) = (foldConst (o, z, f) e1) + (foldConst (o, z, f) e2)
foldConstanta (o, z, f) (e1 :* e2) = (foldConst (o, z, f) e1) + (foldConst (o, z, f) e2)
foldConstanta (o, z, f) (e1 :/ e2) = (foldConst (o, z, f) e1) + (foldConst (o, z, f) e2)
foldConstanta (o, z, f) (Let v e1 e2) = f e2
foldConstanta (o, z, f) (V e) = z

countConst e0 = foldConstanta(o, z, f)
        where
            o = 1
            z = 0
            f e1 = 1 + countConst e1
            
