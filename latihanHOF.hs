-- latihan higher order function
import Data.Maybe
-- Exercises from The Craft of Functional Programming
-- 1.
length' :: [a] -> Int
length' xs = sum(map(\x -> 1) xs)

-- 2.
-- fungsi map paling luar akan menambah satu kepada fungsi map paling dalam, fungsi map paling dalam
-- akan menambah satu dari setiap element dari xs sehingga fungsi tersebut menambah dua (?)

-- 3.
iter :: Int -> (a -> a) -> (a -> a)
iter 0 f x = x
iter n f x = f (iter(n-1) f x)

-- 4. what is type and effect of \n -> iter n succ x
-- akan menjalankan fungsi succ sebanyak n kali
-- Int -> a -> a

-- 5
sumSquare :: Int -> Int
sumSquare n = foldr (+) 0 $ map(\x -> x * x) [1..n]

-- 6. 
{-
(id . f)  is the same as f, because the result of f is fed to the
          identity function id, and thus not changed
(id :: Bool -> Bool)

(f . id)  is the same as f, because the argument of f is fed to the
          identity function id, and thus not changed
(id :: Int -> Int)

id f      is the same as f, because applying id to something does not
          change it
(id :: (Int -> Bool) -> (Int -> Bool))
-}

-- 7.
composeList :: [a -> a] -> (a -> a)
composeList = foldr (.) id 


-- List Comprehensions and Higher-order functions
-- 1. 
func1 xs = map (+1) xs

-- 2.
func2 xs ys = concat(map (\x -> map ( \y -> x + y) ys) xs)

--3.
func3 xs = map (+2) $ filter(>3) xs

-- 4. 
func4 xys = map(\(x,_) -> x + 3) xys

--5.
func5 xys = map (\(x,y) -> x + 3) $ filter (\(x,y) -> (x+y) < 5) xys

-- Way around
-- 1.
around1 xs = [x+3| x <- xs]

--2.
around2 xs = [x| x <- xs, x > 7]

--3.
around3 xs ys = [(x,y)| x <- xs, y <- ys]

--4. 
around4 xys = [x+y| (x,y) <- xys, x + y > 3]
